Function Centroid:Float[](xy:Float[])
	'Ported from the math notation from http://en.wikipedia.org/wiki/Centroid#Centroid_of_polygon
	Local cx:Float = 0.0
	Local cy:Float = 0.0
	Local a:Float = 0.0

	'
	For Local i:Int = 0 To xy.Length - 3 Step 2
		a += 0.5*(xy[i]*xy[i+3] - xy[i+2]*xy[i+1])
	Next
	
	Local cof:Float = 1.0/(6.0*a)
	For Local i:Int = 0 To xy.Length - 3 Step 2
		cx += cof*(xy[i]+xy[i+2])*(xy[i]*xy[i+3] - xy[i+2]*xy[i+1])
	Next
	For Local i:Int = 0 To xy.Length - 3 Step 2
		cy += cof*(xy[i+1]+xy[i+3])*(xy[i]*xy[i+3] - xy[i+2]*xy[i+1])
	Next
	Return [cx,cy]
End


' The rest of this code here is not originally by me (SRJ), but from a library called "Collision" from the Monkey X forums. I can't find it now, (it may not be available anymore?).
Function PointInPoly:Int( point_x:Float, point_y:Float, xy:Float[] )
	' point_x, and point_y are the coordinates of the point to check
	' xy is a list of points, in order, along the border of the polygon
	'
	' For example, the three points of a triangle would be xy = [x1,y1,x2,y2,x3,y3]
	
	If xy.Length<6 Or (xy.Length&1) Return False
	
	Local x1:Float=xy[xy.Length-2]
	Local y1:Float=xy[xy.Length-1]
	Local cur_quad:Int=GetQuad(point_x,point_y,x1,y1)
	Local next_quad:Int
	Local total:Int
	
	For Local i:Int=0 Until xy.Length Step 2
		Local x2:Float=xy[i]
		Local y2:Float=xy[i+1]
		next_quad=GetQuad(point_x,point_y,x2,y2)
		Local diff:Int=next_quad-cur_quad
		
		Select diff
		Case 2,-2
			If ( x2 - ( ((y2 - point_y) * (x1 - x2)) / (y1 - y2) ) )<point_x
				diff=-diff
			Endif
		Case 3
			diff=-1
		Case -3
			diff=1
		End Select
		
		total+=diff
		cur_quad=next_quad
		x1=x2
		y1=y2
	Next
	
	If Abs(total)=4 Then Return True Else Return False
End Function

	'Adapted from LinesCross
Function LinesIntersect:Int[](x0:Float, y0:Float , x1:Float, y1:Float,x2:Float ,y2:Float, x3:Float, y3:Float )
	  
	Local n:Float=(y0-y2)*(x3-x2)-(x0-x2)*(y3-y2)
	Local d:Float=(x1-x0)*(y3-y2)-(y1-y0)*(x3-x2)
	Local out:Int[3]
	out[0] = 0
	out[1] = 0
	out[2] = 0
	
	If Abs(d) < 0.000001 
		' Lines are parallel!
		Return out
	Else
		' Lines might cross!
		Local Sn:Float=(y0-y2)*(x1-x0)-(x0-x2)*(y1-y0)

		Local AB:Float=n/d
		If AB>0.0 And AB<1.0
			Local CD:Float=Sn/d
			If CD>0.0 And CD<1.0
				' Intersection Point
				Local X:Float=x0+AB*(x1-x0)
		       	Local Y:Float=y0+AB*(y1-y0)
				out[0] = 1
				out[1] = X
				out[2] = Y
				Return out
			End If
		End If
	
		' Lines didn't cross, because the intersection was beyond the end points of the lines
	Endif

	' Lines do Not cross!
	Return out

End Function


Function GetQuad:Int(axis_x:Float,axis_y:Float,vert_x:Float,vert_y:Float)
	If vert_x<axis_x
		If vert_y<axis_y
			Return 1
		Else
			Return 4
		Endif
	Else
		If vert_y<axis_y
			Return 2
		Else
			Return 3
		Endif	
	Endif

End Function	

Function Main:Int()
	Return(0)
End