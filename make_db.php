<?php 
$db = new SQLite3("../scores.sqlite");
$query = <<<EOD
  CREATE TABLE IF NOT EXISTS scores (
    name STRING,
    level INTEGER,
    score INTEGER)
EOD;
$db->exec($query) or die('Create db failed');
?>