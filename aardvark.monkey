#MOJO_IMAGE_FILTERING_ENABLED=False
Strict

Import mojo
Import Collision
Import brl.httprequest

Extern
#If TARGET="html5"
'http://www.monkey-x.com/Community/posts.php?topic=1767
'http://diveintohtml5.info/everything.html
Function CanPlayMP3:Bool() = "function (){var a = document.createElement('audio'); return !!(a.canPlayType && a.canPlayType('audio/mpeg;').replace(/no/, ''));}"
Function CanPlayOgg:Bool() = "function (){var a = document.createElement('audio'); return !!(a.canPlayType && a.canPlayType('audio/ogg; codecs=~qvorbis~q').replace(/no/, ''));}"
Global html5_target:Bool = "true"
'#Else
'Global html5_target:Bool = "false"
#End

Public

Global printToScreen:String = ""
Global Rooms:StringMap<Room>
Global ActiveRoom:String =""
'Global ThreadObject:Thread 
Global STARTINGLIVES:Int = 3
Global MAX_LIVES:Int = 8
Global STARTINGLEVEL:Int = 1
Global Score:Int = 0
Global Level:Int = 1


Global Lives:Int = STARTINGLIVES
Global MaxSoundChannel:Int = 31
Global SOUNDEFFECTVOLUME:Float = 0.3

Global HighscoresURL:String = "http://circletheaardvarkinthebackofthepickuptruck.com/submit_high_score.php"
Global GethighscoresURL:String = "http://circletheaardvarkinthebackofthepickuptruck.com/get_high_score.php"
'Global GethighscoresURL:String = "http://php.net/manual/en/function.is-numeric.php"

Global sound_ext:String = ".ogg"
	
'		each aardvark worth 50, multiply by number circled and level: 1 (on level 1) = 1*1*1(50), 2 (on level 3) = 3*2*2(50)
'		
'		Circling Bismarck lowers score by 25% score and cancels out any aardvarks in same circle
'		
'		Infinite Levels, stuff just keeps getting harder: speed, ratio of aardvarks To bismarcks.
' 		Lives: you get 3 Bismarcks
'			Circling a Bismarck cuts your points, and also removes a life
'			At later levels, missing an aardvark also removes a life
'		
'		Need to figure out level progression
'		Need audio for high scores screen
'		Need audio for game over screen
'		Finish timeout graphics
'		IE compatibility (mp3 music) (really strange, don't know what's going on... try different bitrates?)
'		Normalize sound volume
'		fix clock, so when the window isn't active, it doesn't go.

Class Triple<T,Y,U>
''' a class to allow the returning of three different kinds of objects '''
	Field f0:T
	Field f1:Y
	Field f2:U
	
	Method New(p1:T,p2:Y,p3:U)
		f0=p1
		f1=p2
		f2=p3
	End
End

Class Double<T,Y>
''' a class to allow the returning of two different kinds of objects '''
	Field f0:T
	Field f1:Y
	
	Method New(p1:T,p2:Y)
		f0=p1
		f1=p2
	End
End

Class DiscreteDistribution<T>
	'A discrete distribution

	Field ftotal:Int
	Field flist:List<DistributionNode<T>>
	
	Function Test:Bool()
		Local dd:DiscreteDistribution<Int> = New DiscreteDistribution<Int>
		dd.Add(10,10)
		dd.Add(90,90)
		For Local i:Int = 1 To 100
			Print(dd.Pick())
		Next
		Return True
	End
	
	Method New()
		flist = New List<DistributionNode<T>>
	End
	
	Method Add:Void(pobj:T, pweight:Int)
		' negative weights make no sense and are converted into positive weights
		If pweight < 0
			pweight = -1 * pweight
		End
		flist.AddLast(New DistributionNode<T>(pobj, pweight))
		ftotal += pweight
		
	End
	
	Method RemoveEach:Int(pobj:T)
		Local sumremoved:Int = 0
		Local toremove:List<DistributionNode<T>> = New List<DistributionNode<T>>
		Local count:Int = 0
		For Local tmpnode:DistributionNode<T> = Eachin flist
			If tmpnode.Value() = pobj
				sumremoved += tmpnode.Weight()
				toremove.AddLast(tmpnode)
				count += 1
			End
		Next
		ftotal -= sumremoved
		For Local tmpnode:DistributionNode<T> = Eachin toremove
			flist.RemoveEach(tmpnode)
		Next
		Return count
	End
	
	Method Pick:T()
		Local randint:Int = Int(Rnd(ftotal))
		Local sum:Int = 0
		Local prevsum:Int = 0
		Local out:T
'		DebugStop()
		For Local tmpnode:DistributionNode<T> = Eachin flist
			prevsum = sum
			sum = sum + tmpnode.Weight()
			If ((randint >= prevsum) And (randint < sum))
				out = tmpnode.Value()
				Exit 
			End
		Next
		Return out
	End
	
End

Class DistributionNode<T>
	Field fobj:T
	Field fweight:Int
	
	Method New(pobj:T, pweight:Int)
		fobj = pobj
		fweight = pweight
	End
	
	Method Value:T()
		Return fobj
	End
	
	Method Weight:Int()
		Return fweight
	End
End

Function test_threads:Void()
	
	Local xy:Float[] = [0,0, 1.0,1.0, 1.0,3.0, 3,3]
	thread_test_case(xy, -1.0, "four points, but no circle")

	xy = [0,0, 1.0,0.0]
	thread_test_case(xy, -1.0, "two points, so no circle should be formed")

	xy = [0,0, 1.0,1.0, 1.0,3.0]
	thread_test_case(xy, -1.0, "three points, so no circle should be formed")	

	xy = [0,0, 2.0,1.0, 1.0,3.0, 1,0]
	thread_test_case(xy, 0.0, "four points, last point crosses first segment")	
	
	xy = [-1.0,0, 0,0, 2.0,1.0, 1.0,3.0, 1,0]
	thread_test_case(xy, 1.0, "four points, last point crosses second segment ")
	
	xy = [0,2, 0,1, 0,0, 2.0,1.0, 1.0,3.0, 1,0]
	thread_test_case(xy, 1.0, "four points, last point crosses third segment")

	xy = [0,10, 0,9, 0,8, 0,0, 2.0,1.0, 1.0,3.0, 1,0]
	thread_test_case(xy, 1.0, "four points, last point crosses fourth segment")
	
	xy = [-2,0, 2,0, 5,2, 2,5, 2.0,-3.0]
	thread_test_case(xy, 0.0, "five points, cross at intersection")
	
End

Function thread_test_case:Void(pxy:Float[], expected:Float, description:String, tol:Float = 0.01)
	Local t:Thread = New Thread()
	
	Print(description)	
	For Local i:Int = 0 To pxy.Length - 2 Step 2
		'Print(String(pxy[i]) + " " + String(pxy[i+1]))
		t.pushPoint(pxy[i], pxy[i+1])
	Next
	Local sol:Point = t.closed()
	Print(String(sol.fq))
	
End

Class Point
	Field fx:Float
	Field fy:Float
	Field fq:Float 'another field... you know in case you're into that sort of thing...
	
	Method New(px:Float, py:Float, pq:Float = 0)
		fx = px
		fy = py
		fq = pq
	End
	
	Method abs:Float()
		'distance between the point and the origin
		Return Sqrt(Pow(fy,2) + Pow(fx,2))
	End
	
	Function dist:Float(p1:Point, p2:Point)
		'distance between two points
		Local dy:Float = p2.fy - p1.fy
		Local dx:Float = p2.fx - p1.fx
		
		Return Sqrt(Pow(dy,2) + Pow(dx,2))
	End
	
	'dot product of two points (ie vectors)
	Function dot:Float(p1:Point, p2:Point)
		Return (p1.fx * p2.fx) + (p1.fy * p2.fy)
	End
	
	Function distenum:Float(x1:Float,y1:Float,x2:Float,y2:Float)
		'distance between two points, given x and y coordinates
		Local dy:Float = y2 - y1
		Local dx:Float = x2 - x1
		
		Return Sqrt(Pow(dy,2) + Pow(dx,2))
	End

	Function pointOnSegment:Bool(p:Point, a:Point, b:Point)
		Local tol:Float = 0.001
		Local sensitivity:Float = Sqrt(2) + 0.001 'add a little bit just to be on the safe side
		Local dist:Float = sensitivity + 1 'something bigger than sensitivity
		
		Local v1:Point = New Point(p.fx-a.fx,p.fy-a.fy)
		Local v2:Point = New Point(b.fx-a.fx,b.fy-a.fy)
		
		Local dp:Float = Point.dot(v1,v2)
		
		Local dpa:Float = Point.dist(p,a)
		
		If (dpa < 1)
			dist = dpa
		Else If (dp > 0)
			Local cos:Float = dp/(v1.abs()*v2.abs())
			Local c:Float = cos * v1.abs()
			Local projRatio:Float = c/v2.abs()
			If (projRatio >= 1)
				dist = Point.dist(p,b)
			Else
				Local v3:Point = New Point((dp/Pow(v2.abs(),2))*v2.fx,(dp/Pow(v2.abs(),2))*v2.fy)
				dist = Point.dist(v3,v1)
			End
		Else
			dist = dpa
		Endif 
		
		If (dist <= sensitivity)
			printToScreen = String(dist)	
			Return True
		Else
			Return False
		Endif
				
	End

	Function linesTouch:Int[](p:Point, q:Point, a:Point, b:Point)
		Local out:Int[3]
		out[0] = 0
		out[1] = 0
		out[2] = 0
		
		If pointOnSegment(p,a,b)
			'Print("on")
			out[0] = 1
			out[1] = p.fx
			out[2] = p.fy
		Elseif pointOnSegment(a,p,q)
			'Print("inverse_on")
			out[0] = 1
			out[1] = a.fx
			out[2] = a.fy
		Else		
			out = Collision.LinesIntersect(p.fx,p.fy,q.fx,q.fy,a.fx,a.fy,b.fx,b.fy)
			If out[0] = 1
				'Print("cross")		
			Endif
		Endif
		
		Return out
	
	End
End


Interface Room
	Method mouseenabled:Bool()
	Method threadsenabled:Bool() 
	Method pauseenabled:Bool()
	
	Method load:Void(ptag:String,  pval:String) 'allocate room resources
	
	Method unload:Void() 'deallocate room resources
	
	Method activate:Void() 'call once room resources have been loaded and the room should become visible
	
	Method render:Void() ' render the room graphics
	
	
	Method update:Void() 'process room logic
	
End


Class TitleRoom Implements Room
	Field fmouseenabled:Bool = True
	Field fthreadsenabled:Bool = False
	Field fpauseenabled:Bool = False
	
	Method mouseenabled:Bool()
		Return fmouseenabled
	End
	
	Method threadsenabled:Bool() 
		Return fthreadsenabled
	End
	
	Method pauseenabled:Bool()
		Return fpauseenabled
	End
	
	Const MUSIC:String = "introMusic"
	Const BACKGROUNDIMAGE:String = "TitleScreen.png"
	Field fbackground:Image
	
	
	Method New()
	
	End
	
	Method load:Void(ptag:String = "", pval:String = "")
		fbackground = LoadImage(BACKGROUNDIMAGE)
	End
	
	Method activate:Void()
		my_PlayMusic(MUSIC,1)
	End
	
	Method render:Void()
		DrawImage(fbackground,0,0)
	End

	Method update:Void()
		If (pen.hit())
			'printToScreen = "Hit " + String(pen.coords.fx) + " " + String(pen.coords.fy)
			If(pen.coords.fx > 12 And pen.coords.fx < 102 And pen.coords.fy > 426 And pen.coords.fy < 466)
				ResetGame()
				ChangeRoom("Lot")
			Elseif (pen.coords.fx > 116 And pen.coords.fx < 204 And pen.coords.fy > 424 And pen.coords.fy < 468)
				'Help
			Elseif (pen.coords.fx > 249 And pen.coords.fx < 357 And pen.coords.fy > 427 And pen.coords.fy < 470)
				ChangeRoom("Highscores")
				'Highscore
			Endif
		Endif
	End
	
	Method unload:Void()
		fbackground = Null
	End
End

Class GameoverRoom Implements Room, IOnHttpRequestComplete
	Field fmouseenabled:Bool = True
	Field fthreadsenabled:Bool = False
	Field fpauseenabled:Bool = False
	
	Method mouseenabled:Bool()
		Return fmouseenabled
	End
	
	Method threadsenabled:Bool() 
		Return fthreadsenabled
	End
	
	Method pauseenabled:Bool()
		Return fpauseenabled
	End
	
	Const MUSIC:String = "Cascade0" 'TODO: get some game-over music
	Const BACKGROUNDIMAGE:String = "game_over.png"
	Field fbackground:Image
	Field fname:String
	Field fscorestatus:String '"notsent", "sent", "recorded", "error"
	
	Method New()
	
	End
	
	Method load:Void(ptag:String = "", pval:String = "")
		fbackground = LoadImage(BACKGROUNDIMAGE)
	End
	
	Method activate:Void()
		my_PlayMusic(MUSIC,1)
		fname = ""
		fscorestatus = "notsent"
	End
	
	Method render:Void()
		DrawImage(fbackground,0,0)
		Render.text(String(Level), 398, 231, 2.0)
		Render.text(String(Score), 397, 267, 2.0)
		Render.text(fname, 380, 314, 2.0)
		
		If fscorestatus="error"
			Render.text("Error sending score", 293, 347, 1.5)
		Elseif fscorestatus="recorded"
			Render.text("Score Sent!", 293, 347, 1.5)		
		Endif
	End

	Method update:Void()
		Repeat
			Local char:Int=GetChar()
			If Not char Exit
			If ((char >= 65 And char <= 90) Or (char >= 97 And char <= 122) Or (char >= 48 And char <= 57)) And fname.Length < 12 'char is alphanumeric and the string is less than 12 characters long
				fname += String.FromChar(char)
			Endif
			If (char = 127 Or char = 8) And fname.Length > 0
				fname = fname[..(fname.Length-1)]
			Endif
		Forever
		If (pen.hit())
			If(pen.coords.fx > 327 And pen.coords.fx < 427 And pen.coords.fy > 409 And pen.coords.fy < 462)
				'RETRY
				Score = 0
				Level = 1
				Lives = STARTINGLIVES
				ChangeRoom("Lot")
			Elseif (pen.coords.fx > 472 And pen.coords.fx < 622 And pen.coords.fy > 403 And pen.coords.fy < 460)
				'MAIN MENU
				ResetGame()
				ChangeRoom("Title")
			Elseif (pen.coords.fx > 468 And pen.coords.fx < 621 And pen.coords.fy > 349 And pen.coords.fy < 376)
				'SUBMIT
				If fscorestatus = "notsent" Or fscorestatus = "error" 
					If fname = ""
						fname = " "
					endif
					Local req:HttpRequest = New HttpRequest("POST", HighscoresURL, Self)
					req.Send("name=" + String(fname) + "&"+"level="+String(Level) +"&"+"score="+String(Score), "application/x-www-form-urlencoded")
					fscorestatus="sent"
				Endif
			Endif
		Endif
		If fscorestatus = "sent"
			UpdateAsyncEvents()
		Endif
	End
	
	Method unload:Void()
		fbackground = Null
	End
	
	Method OnHttpRequestComplete:Void(request:HttpRequest)
		'TODO:validate the message returned by the server
		If request.Status() = -1
			fscorestatus="error"
		Else
			Local req_text:String = request.ResponseText()
			req_text = req_text.Trim()
			Print(req_text)
			If req_text = "Success!"
				fscorestatus="recorded"
				ChangeRoom("Highscores")			
			Else
				fscorestatus="error"
			Endif

		Endif
	End
End

Class HighscoresRoom Implements Room, IOnHttpRequestComplete
	Field fmouseenabled:Bool = True
	Field fthreadsenabled:Bool = False
	Field fpauseenabled:Bool = False
	
	Method mouseenabled:Bool()
		Return fmouseenabled
	End
	
	Method threadsenabled:Bool() 
		Return fthreadsenabled
	End
	
	Method pauseenabled:Bool()
		Return fpauseenabled
	End
	
	Const MUSIC:String = "Cascade0" 'TODO: get some High-scores music
	Const BACKGROUNDIMAGE:String = "high_scores.png"
	Field fbackground:Image
	Field fscorestring:String 'what the server returns from the request
	Field fscorestatus:String '"loading", "error", "got"
	Field fscores:String[] 'list of scores of the high-scorers
	Field fnames:String[] 'list of names of the high-scorers
	Const SCOREYSPACE:Float = 35 'may want to replace this with some function of font height eventually
	Const SCORESTOSHOW:Int = 12 'How many lines of scores to print
	
	Method New()
	
	End
	
	Method load:Void(ptag:String = "", pval:String = "")
		Local req:HttpRequest = New HttpRequest("GET", GethighscoresURL, Self)
		req.Send()
		fbackground = LoadImage(BACKGROUNDIMAGE)
	End
	
	Method activate:Void()
		my_PlayMusic(MUSIC,1)
		fscorestring = ""
		fscorestatus="loading"
	End
	
	Method render:Void()
		DrawImage(fbackground,0,0)
		If fscorestatus="loading"
			Render.text("Loading Scores", 200, 108, 3.0)
		Elseif fscorestatus="error"
			Render.point_centered_text("Error retrieving high scores", 299, 108, 3)
		Elseif fscorestatus="got"
			render_scores()
		Endif
	End

	Method render_scores:Void()
		Local smallest:Int = fscores.Length
		If fnames.Length < smallest 'this should never be necessary, but it's here to avoid an out of bounds error on a freak accident
			smallest = fnames.Length
		Endif
		Local y:Float = 80.0
		
		If smallest > SCORESTOSHOW
			smallest = SCORESTOSHOW
		End
		
		For Local i:Int = 0 To smallest - 1
			Render.text(fnames[i], 70.0, y, 2.0)
			Render.text(fscores[i], 360.0, y, 2.0)
			y += SCOREYSPACE
		Next
	End

	Method parse_scores:Void(scores_text:String)
		'Print(scores_text)
		Local names_text_list:String = ""
		Local scores_text_list:String = ""
		scores_text = scores_text.Replace("~r", "")
		scores_text = scores_text.Trim()
		Local lines:String[] = scores_text.Split("~n")
		For Local line:String = Eachin lines
			Local parts:String[] = line.Split("~t")
			names_text_list = names_text_list + parts[0] + "~t"
			scores_text_list = scores_text_list + parts[1] + "~t"
		Next
		fscores = scores_text_list.Trim().Split("~t")
		fnames = names_text_list.Trim().Split("~t")
	End


	Method update:Void()
		If (pen.hit())
			If(pen.coords.fx > 449 And pen.coords.fx < 521 And pen.coords.fy > 414 And pen.coords.fy < 474)
				'RETRY
				ResetGame()
				ChangeRoom("Lot")
			Elseif (pen.coords.fx > 536 And pen.coords.fx < 620 And pen.coords.fy > 412 And pen.coords.fy < 474)
				'MAIN MENU
				ChangeRoom("Title")
			Endif
		Endif
		If fscorestatus="loading"
			UpdateAsyncEvents()
		Endif
	End
	
	Method unload:Void()
		fbackground = Null
	End
	
	Method OnHttpRequestComplete:Void(request:HttpRequest)
		If request.Status() = -1
			fscorestatus="error"
		Else
			Local scores_page_text:String = request.ResponseText()
			'Print(GethighscoresURL)
			'Print(request.ResponseText())
			'Print(String(request.Status()))
			If scores_page_text <> "Error"
				fscorestatus="got"
				'scores_page_text = "bug3000~t100~r~nAardvarkusRex~t500~r~nMilo~t1124556" 'comment this line out once the web code works
				parse_scores(scores_page_text) 'If there's an error, parse_scores will overwrite fscorestatus to be "error"
			Else
				fscorestatus="error"
			Endif
		Endif
	End
End

Class LoadRoom Implements Room
	Field fmouseenabled:Bool = True
	Field fthreadsenabled:Bool = False
	Field fpauseenabled:Bool = False
	
	Method mouseenabled:Bool()
		Return fmouseenabled
	End
	
	Method threadsenabled:Bool() 
		Return fthreadsenabled
	End
	
	Method pauseenabled:Bool()
		Return fpauseenabled
	End
	
	
	'Const MUSIC:String = "Cascade0.ogg"
	'Const BACKGROUNDIMAGE:String = "TitleScreen.png"
	Field fbackground:Image
	Field fnextroom:String
	Field fnextval:String
	Field frendered:Bool 
	Field fprevroom:String
	
	Method New()
	
	End
	
	Method load:Void(ptag:String, pval:String = "")
		'fbackground = LoadImage(BACKGROUNDIMAGE)
		frendered = False
		fnextroom = ptag
		fnextval = pval
		fprevroom = ActiveRoom
		ActiveRoom = "Load"
	End
	
	Method activate:Void()

	End
	
	Method render:Void()
		Render.text("Loading, Please Wait...",25,25,2)
		frendered = True
	End

	Method update:Void()
		If (frendered)
			If (fprevroom <> "")
				Rooms.Get(fprevroom).unload()
			Endif
			Rooms.Get(fnextroom).load(fnextval,"")
			Rooms.Get(fnextroom).activate()			
			ActiveRoom = fnextroom
		End
	End
	
	Method unload:Void()
	
	End
End



Class LotRoom Implements Room
	Field fmouseenabled:Bool = True
	Field fthreadsenabled:Bool = True
	Field fpauseenabled:Bool = True
	Field fshowscore:Bool = True
	Field ftimer:Timer
	Field fgrid:Grid
	Field fscoretimer:Timer
	Field flastscore:Int

	
	Method mouseenabled:Bool()
		Return fmouseenabled
	End
	
	Method threadsenabled:Bool() 
		Return fthreadsenabled
	End
	
	Method pauseenabled:Bool()
		Return fpauseenabled
	End
	
	
	Const MUSIC:String = "mainMusic"
	Const BACKGROUNDIMAGE:String = "ParkingLot.png"
	Const HEARTIMAGE:String = "heart.png"
	Const REDFONTIMAGE:String = "smallerfont_outlined_red.png"
	Const BLUEFONTIMAGE:String = "smallerfont_outlined_blue.png"
	Const TIMEOUTLEVEL:Int = 5 'at levels equal or higher than this, when an aardvark times out, the player loses a life
	Const TIMEOUTSOUND:String = "buzz.wav"
	Field fredfont:Font
	Field fbluefont:Font
	Field ftimeoutsound:Sound
	Field fbackground:Image
	Field fheart:Image
	Field fcirclecenter:Point
	Field state:String '"start", "play", "end"
	
	Method New()
	
	End
	
	Method load:Void(ptag:String, pval:String = "")
		fbackground = LoadImage(BACKGROUNDIMAGE)
		fheart = LoadImage(HEARTIMAGE)
		fredfont = New Font(REDFONTIMAGE)
		fbluefont = New Font(BLUEFONTIMAGE)
		ftimeoutsound = my_LoadSound("buzz")
		setup_board()
	End
	
	Method activate:Void()
		
	End
	
	Method setup_board:Void()
		fgrid = New Grid("Lot", Level)
		state = "start"
	End
	
	Method start_round:Void()
		my_PlayMusic(MUSIC,1)
		SetMusicVolume(0.2)
		fscoretimer = New Timer(2000, False)
		flastscore = 0
		fcirclecenter = New Point(0,0)
		ftimer = New Timer(Game.ROUND_TIME)
	End	
	
	Method stop_round:Void()
		StopMusic()
		state = "end"
	End

	
	Method render:Void()
		DrawImage(fbackground,0,0)
		If state = "start"
			Render.point_centered_text("Level: " + String(Level), Game.WIDTH/2.0, (Game.HEIGHT/5.0) * 1.0, 3.0)
			Render.point_centered_text("Score: " + String(Score), Game.WIDTH/2.0, (Game.HEIGHT/5.0) * 2.0, 3.0)
			Render.point_centered_text("Click To Start", Game.WIDTH/2.0, (Game.HEIGHT/5.0) * 4.0, 4.0, fredfont)
		Elseif state = "end"
			Render.point_centered_text("Level Complete!", Game.WIDTH/2.0, (Game.HEIGHT/5.0) * 1.0, 4.0, fbluefont)
			Render.point_centered_text("Score: " + String(Score), Game.WIDTH/2.0, (Game.HEIGHT/5.0) * 2.0, 3.0)
			Render.point_centered_text("Click To Continue", Game.WIDTH/2.0, (Game.HEIGHT/5.0) * 4.0, 4.0, fredfont)
		Elseif state = "play"
			
			'DrawText("Room",10,10)
			fgrid.render()
			For Local i:Int = 1 To Lives
				DrawImage(fheart, 10*i + (i-1)*fheart.Width(), Game.HEIGHT-(fheart.Height()+10))
			Next
			
			If (Not(ftimer.expired()))  'If timer is going, draw it
				Render.num(ftimer.secsleft(),3,0.5, 5, 5)
			Endif
	
			If (Not(fscoretimer.expired()) And flastscore <> 0)
				Local render_font:Font = fbluefont
				Local render_score:Int = flastscore
				If flastscore < 0
					render_font = fredfont
					'render_score = -1 * flastscore
				End
				Render.point_centered_text(String(render_score), fcirclecenter.fx, fcirclecenter.fy, 2.5, render_font)
			End
			
			Render.text("Level:" + String(Level), Game.WIDTH-165, Game.HEIGHT-25, 2.0)
			
			If (fshowscore)
				'Render.num(Score,7,0.5,5,5)
				Render.rnum(Score,0.5,Game.WIDTH-5,5)
			Endif
		Endif
	End

	Method update:Void()
		If state = "start"
			If TouchHit( 0 )
				start_round()
				state = "play"
			Endif
		Elseif state = "end"
			If TouchHit( 0 )
				Level += 1
				setup_board()
			Endif
		Elseif state = "play"
			Local changedspaces:List<Double<String,String>> = fgrid.update()  'returns a list of new states and target types
			
			For Local spc:Double<String,String> = Eachin changedspaces 
				If Level >= TIMEOUTLEVEL And spc.f0 = "timedout" And spc.f1 = "points"
					Lives -= 1
					global_playsound(ftimeoutsound)
				Endif 
			Next
			
			If Threads.circleformed
				'get a list of circled targets and their point values
				Local circled:List<Triple<String, Int, Float>> = fgrid.circle(Threads.circlexy)
				
				'set a place to display the score of the new circle, and set the timer to display the score
				Local centroid:Float[] = Collision.Centroid(Threads.circlexy)
				fcirclecenter.fx = centroid[0]
				fcirclecenter.fy = centroid[1]		
				fscoretimer.reset()
				
				
	'			Local newscore:Int = Score + delta_score
				Local livesdelta:Int = 0
				Local scoremultiplier:Float = 1.0
				Local negativemultiplier:Float = 1.0
				Local deltascore:Int = 0
				Local pointtargs:Int = 0
				
				For Local temptarg:Triple<String, Int, Float> = Eachin circled 'target_type, points, multiplier

					deltascore += temptarg.f1
					If temptarg.f0 = "life"
						livesdelta += 1
					Endif
					If temptarg.f2 < 1.0
						livesdelta -= 1
						negativemultiplier *= temptarg.f2
					Elseif temptarg.f2 > 1.0
						scoremultiplier *= temptarg.f2
					Endif
					If temptarg.f1 > 0
						pointtargs += 1
					Endif
				Next
		
				If negativemultiplier < 0.999
					deltascore = -1 * (Score * (1.0 - negativemultiplier))
				Else
					If deltascore > 0
						deltascore = deltascore * scoremultiplier * Level * pointtargs
					Else
						'I'm not sure...
					End
				End
	
				flastscore = deltascore
				Local newscore:Int = Score + deltascore
				If newscore < 0 
					Score = 0
				Else
					Score = newscore
				End
				
				If livesdelta >= 0
					If Lives + livesdelta <= MAX_LIVES
						Lives = Lives + livesdelta
					Else
						Lives = MAX_LIVES
					Endif
				Elseif -1 * livesdelta <= Lives
					Lives += livesdelta
				Else
					Lives = 0
				Endif
			End
			If Lives <= 0
				Lives = 0
				ChangeRoom("Gameover")
			Endif
			If ftimer.expired()
				stop_round()
			Endif
		Endif
	End
	
	Method unload:Void()
		fbackground = Null
		ftimer.remove()
	End
End

Class Sprite
	Field fimage:Image 
	Field fframes:Int
	Field fanimspeed:Int 'frames per second, 0 for static image
	Field fanimtimer:Timer
	Field fframe:Int
	Field fscale:Float
	Field frepeat:Bool
	Field fxhandle:Float
	Field fyhandle:Float
	Global imagecache:StringMap<IntMap<Image>> = New StringMap<IntMap<Image>>
	'first key is image name, second key is number of frames
	
	Method New(pimagepath:String, pframes:Int=1, panimspeed:Int = 0, pscale:Float = 1, pmidhandle:Bool = False, prepeat:Bool = False)
		If Not imagecache.Contains(pimagepath)
			imagecache.Set(pimagepath, New IntMap<Image>)
		End
		If Not imagecache.Get(pimagepath).Contains(pframes)
			imagecache.Get(pimagepath).Set(pframes, LoadImage(pimagepath, pframes))
		End
		
		fimage = imagecache.Get(pimagepath).Get(pframes)
		
		fframes = pframes
		fxhandle = 0.0;
		fyhandle = 0.0;
		
		If pmidhandle
			fxhandle = Float(fimage.Width()/2.0)
			fyhandle =  Float(fimage.Height()/2.0)
		End
		fanimtimer = New Timer()
		Self.changeanimationspeed(panimspeed)
		fscale = pscale
		frepeat = prepeat
	End
	
	Method startanimation:Void()
		fanimtimer.unpause()
	End
	
	Method stopanimation:Void()
		fanimtimer.pause()
	End
	
	Method changeanimationspeed:Void(pfps:Int)
		fanimspeed = pfps
		If pfps <> 0
			fanimtimer.set(1000/pfps, fanimtimer.factive)
		Else
			fanimtimer.reset()
			fanimtimer.pause()
		End
	End
	
	Method resetanimation:Void()
		fframe = 0
		ftimer.reset()
	End
	
	Method render:Void(px:Float, py:Float)
		'draw current frame, then advance the frame and reset the animation timer if the timer has expired and animspeed is not 0
		DrawImage(fimage, px-fxhandle, py-fyhandle, 0, fscale, fscale, fframe)

		If ((fanimspeed <> 0) And (fanimtimer.factive) And (fanimtimer.expired()))
			fframe = fframe + 1
			fanimtimer.reset()
			If fframe >= fimage.Frames()
				If frepeat
					fframe = 0
				Else
					fframe = fimage.Frames() - 1
					Self.stopanimation()
				End
			End
		End
		
	End
End	




Class Target
	
	Field fxdrawoffset:Float
	Field fydrawoffset:Float
	Field fytargetoffset:Float
	Field fxtargetoffset:Float
	Field fptvalue:Int 'can be positive or negative
	Field fptmultiplier:Float 'should be positive
	
	Field ftype:String 'the name of the target type (aardvark, bismarck, newlife... etc)
	Field fsprites:StringMap<Sprite>
 	Field fsounds:StringMap<Sound> 'sound to play on the associated 
	Field fclass:String 'the name of the target class (points, powerup, bad)
	
	'Function initialize:Void()
	'	fconfettiimg = LoadImage("confetti.png",28)
	'	faardvarkimg = LoadImage("aardvark_sprite_transparency.png")
	'	fbismarckimg = LoadImage("bismarck.png")
	'End
	
	'Field toggle:Bool
	
	Method New(type:String)
	
		''' intialize values and set defaults '''
		fsprites = New StringMap<Sprite>
		fsounds = New StringMap<Sound>
		fptvalue = 0
		fptmultiplier = 1.0
		fxdrawoffset = 0'(-1 * fimage.Width())
		fydrawoffset = 0 '((-1 * fimage.Height()) + 26)
		fytargetoffset = 0 'fydrawoffset/2
		fxtargetoffset = 0 '(fimage.Width()/2)

		
		''' load specific target type '''
		switchType(type)
	End
	
	Method resetanimations:Void()
		fspawnsprite.resetanimation()
		factivesprite.resetanimation()
		fcircledsprite.resetanimation()
		ftimedoutsprite.resetanimation()
	End
	
	Method animationfinished:Bool(pstate:String)
		Local out:Bool = True
'		Print(pstate)
		If fsprites.Contains(pstate)
			out = Not fsprites.Get(pstate).fanimtimer.factive
		Endif
		Return out
	End
	
	Method draw:Void(px#, py#, pstate:String) 'returns true if it's an animation and has ended
		If fsprites.Contains(pstate)
			fsprites.Get(pstate).render(px+fxdrawoffset, py+fydrawoffset)
		End
	End
	
	Method playsound:Void(pstate:String)
		If fsounds.Contains(pstate)
			global_playsound(fsounds.Get(pstate))
		End
	End
	
	Method switchType:Void(ptype:String)
		ftype = ptype
		'Print(ftype)
		Select ptype
			Case "aardvark"
				fsprites.Set("spawn", New Sprite("hiding_aardvark_sprite.png",1,0,1, True, False))
				fsprites.Set("active", New Sprite("aardvark_stand.png",6,30,1, True, False))
				fsprites.Set("circled", New Sprite("confetti.png",28,30,1, True, False))
				fsprites.Set("timedout", New Sprite("aardvark_squat.png",10,30,1, True, False))
				
				fsounds.Set("circled", my_LoadSound("pop"))
				
				fclass = "points"
				fxdrawoffset = 0'(-1 * fimage.Width())
				fydrawoffset = 0 '((-1 * fimage.Height()) + 26)
				fytargetoffset = 0 'fydrawoffset/2
				fxtargetoffset = 0 '(fimage.Width()/2)
				fptvalue = 50
			Case "bismarck"
				fsprites.Set("spawn", New Sprite("hiding_bismarck_sprite.png",1,0,1, True, False))
				fsprites.Set("active", New Sprite("bismarck_stand.png",7,30,1, True, False))
				fsprites.Set("circled", New Sprite("confetti.png",28,30,1, True, False))
				fsprites.Set("timedout", New Sprite("bismarck_squat.png",10,30,1, True, False))
				
				fsounds.Set("circled", my_LoadSound("buzz"))
				
				fclass = "bad"
				fxdrawoffset = 0'(-1 * fimage.Width())
				fydrawoffset = 0 '((-1 * fimage.Height()) + 26)
				fytargetoffset = 0 'fydrawoffset/2
				fxtargetoffset = 0 '(fimage.Width()/2)
				fptvalue = 0
				fptmultiplier = 0.75
			Case "bisvark"
				fsprites.Set("spawn", New Sprite("hiding_bismarck_sprite.png",1,0,1, True, False))
				fsprites.Set("active", New Sprite("bisvark_stand.png",7,30,1, True, False)) 
				fsprites.Set("circled", New Sprite("confetti.png",28,30,1, True, False))
				fsprites.Set("timedout", New Sprite("bisvark_squat.png",10,30,1, True, False))
				
				fsounds.Set("circled", my_LoadSound("pop"))

				fclass = "points"				
				fxdrawoffset = 0'(-1 * fimage.Width())
				fydrawoffset = 0 '((-1 * fimage.Height()) + 26)
				fytargetoffset = 0 'fydrawoffset/2
				fxtargetoffset = 0 '(fimage.Width()/2)
				fptvalue = 75
			Case "aardmarck"
				fsprites.Set("spawn", New Sprite("hiding_aardmarck_sprite.png",1,0,1, True, False))
				fsprites.Set("active", New Sprite("aardmarck_stand.png",6,30,1, True, False)) 
				fsprites.Set("circled", New Sprite("confetti.png",28,30,1, True, False))
				fsprites.Set("timedout", New Sprite("aardmarck_squat.png",10,30,1, True, False)) 
				
				fsounds.Set("circled", my_LoadSound("buzz"))
				
				fclass = "bad"
				fxdrawoffset = 0'(-1 * fimage.Width())
				fydrawoffset = -11 '((-1 * fimage.Height()) + 26)
				fytargetoffset = -11 'fydrawoffset/2
				fxtargetoffset = 0 '(fimage.Width()/2)
				fptvalue = 0
				fptmultiplier = 0.75
				
			Case "life"
'				fsprites.Set("spawn", New Sprite("hiding_aardvark_sprite.png",1,0,1, True, False))
				fsprites.Set("active", New Sprite("heart.png",1,0,1, True, False)) 
				fsprites.Set("circled", New Sprite("heart_fade.png",5,30,1, True, False))
'				fsprites.Set("timedout", New Sprite("confetti.png",28,30,1, True, False)) 
				
				fsounds.Set("circled", my_LoadSound("get_a_life"))

				fclass = "powerup"				
				fxdrawoffset = 0'(-1 * fimage.Width())
				fydrawoffset = -5 '((-1 * fimage.Height()) + 26)
				fytargetoffset = -5 'fydrawoffset/2
				fxtargetoffset = 0 '(fimage.Width()/2)
				fptvalue = 0
				fptmultiplier = 1.0

			Case "bonus_points"
				fsprites.Set("active", New Sprite("bonus_pts.png",1,0,1, True, False)) 
				fsprites.Set("circled", New Sprite("bonus_pts_fade.png",10,30,1, True, False))
				
				fsounds.Set("circled", my_LoadSound("pop")) 'TODO: better sound for this

				fclass = "powerup"				
				fxdrawoffset = 0'(-1 * fimage.Width())
				fydrawoffset = -5 '((-1 * fimage.Height()) + 26)
				fytargetoffset = -5 'fydrawoffset/2
				fxtargetoffset = 0 '(fimage.Width()/2)
				fptvalue = 0
				fptmultiplier = 2.0
		End
		
	End
End

Function my_LoadSound:Sound(pname:String)
	'Print(pname + sound_ext)
	Return LoadSound(pname + sound_ext)
End

Function my_PlayMusic:Int(ppath:String, pflags:Int=1)
	'Print(ppath+sound_ext)
	Return PlayMusic(ppath+sound_ext, pflags)
End

Function global_set_sound_volume:Void(pvolume:Float)
	Local channel:Int = 0	
	While channel <= MaxSoundChannel
		SetChannelVolume(channel, pvolume)
		channel += 1
	Wend 
End

Function global_playsound:Void(sound:Sound)
	Local channel:Int = 0
	While ChannelState(channel) = 1 And channel < MaxSoundChannel
		channel += 1
	Wend 
	PlaySound(sound, channel)
End

Class Grid
	Field froom:String
	Field femptyspaces:List<GridSpace>
	Field fusedspaces:List<GridSpace>
	'Field fpercentbad:Int = 0
	Field faveragespawntime:Float = 1000 'millisecs
	Field fspawntimecenterweight:Int = 5 'must be at least 1. A measure of how center weighted the spawn time distribution is.
	'At low difficulty levels, average spawn time and center weight should both be high. At more difficult levels, spawning should get faster and more erratic, by decreasing both numbers
	Field fspawnclock:Timer
	Field fspawndistribution:DiscreteDistribution<String>
	Field fspawntimedistribution:DiscreteDistribution<Int>
	Field factivetimedistribution:DiscreteDistribution<Int>
	Const EARS_TIME:Int = 500
	
	Method New(proom:String, plevel:Int)
		froom = proom
		femptyspaces = New List<GridSpace>
		fusedspaces = New List<GridSpace>
		fspawndistribution = New DiscreteDistribution<String>
		fspawntimedistribution = New DiscreteDistribution<Int>
		factivetimedistribution = New DiscreteDistribution<Int>
		fspawnclock = New Timer(100) 'number doesn't matter because it gets overwritten in the next line
		loadLevel(froom, plevel)
	End
	
	Method update:List<Double<String,String>>() 'a list of new states and target types
		Local out:List<Double<String,String>> = New List<Double<String,String>>
		If(fspawnclock.expired())
			spawn()
			setspawnclock()
		End
		
		Local spacestoremove:List<GridSpace> = New List<GridSpace>
		
		For Local tmpSpace:GridSpace = Eachin fusedspaces
			If tmpSpace.stateexpired()

				Local newstate:String = ""
				Select tmpSpace.fstate
					Case "spawn"
						tmpSpace.statetransition("active",factivetimedistribution.Pick())						
						newstate = "active"
					Case "active"
						tmpSpace.statetransition("timedout",10000) 'number at the end is arbitrary
						newstate = "timedout"
					Case "circled", "timedout"
						spacestoremove.AddLast(tmpSpace)
						newstate = "none"
				End
				out.AddLast(New Double<String, String>(newstate, tmpSpace.ftarget.fclass))
			End
		Next
		
		For Local tmpSpace:GridSpace = Eachin spacestoremove
			'Print(String(tmpSpace.fid))
			fusedspaces.RemoveFirst(tmpSpace)
			femptyspaces.AddLast(tmpSpace)
		Next
		Return out
	End
	
	Method spawn:Void()
		Local spacesleft:Int = femptyspaces.Count()
		If spacesleft <> 0 	' only spawn if there are spaces to spawn
			Local spacetofill:Int = Int(Rnd(spacesleft))
			Local counter:Int
			Local tmpspace:GridSpace
			For Local myspace:GridSpace=Eachin femptyspaces
				If counter = spacetofill
					tmpspace=myspace
				End
				counter = counter + 1
			Next
			femptyspaces.RemoveFirst(tmpspace)
			tmpspace.activate(fspawndistribution.Pick(), EARS_TIME)
			fusedspaces.AddLast(tmpspace)
		
		End
	End
	
	Method loadLevel:Void(proom:String, pdifficulty:Int)
		Select proom
			Case "Lot"
				'top row
				addspace(99, 89, 0) 
				addspace(332, 87, 1)
				addspace(546, 87, 2)
				
				'middle row
				addspace(99, 239, 3)
				addspace(332, 237, 4)
				addspace(546, 237, 5)
				
				'bottom row
				addspace(99, 385, 6)
				addspace(332, 383, 7)
				addspace(546, 383, 8)
				
				Select pdifficulty
					Case 1
						'fspawndistribution.Add("life", 30)
						'fspawndistribution.Add("aardmarck", 30)

'						fspawndistribution.Add("aardvark", 80)
						fspawndistribution.Add("aardvark", 70)
						fspawndistribution.Add("bismarck", 30)
						'fspawndistribution.Add("bisvark", 30)
						
						fspawntimedistribution.Add(500, 10)
						fspawntimedistribution.Add(750, 50)
						fspawntimedistribution.Add(1500, 30)
						fspawntimedistribution.Add(2000, 10)
						
						
						factivetimedistribution.Add(1000, 10)
						factivetimedistribution.Add(1500, 15)
						factivetimedistribution.Add(4000, 50)
						factivetimedistribution.Add(5000, 15)
						factivetimedistribution.Add(7000, 10)
						

					Case 2
						fspawndistribution.Add("aardvark", 70)
						fspawndistribution.Add("bismarck", 20)
						fspawndistribution.Add("bisvark", 10)
						fspawntimedistribution.Add(400, 10)
						fspawntimedistribution.Add(500, 50)
						fspawntimedistribution.Add(1200, 30)
						fspawntimedistribution.Add(2000, 10)
						
						
						factivetimedistribution.Add(1000, 10)
						factivetimedistribution.Add(1500, 15)
						factivetimedistribution.Add(4000, 50)
						factivetimedistribution.Add(5000, 15)
						factivetimedistribution.Add(7000, 10)
					Case 3
						fspawndistribution.Add("aardvark", 60)
						fspawndistribution.Add("bismarck", 25)
						fspawndistribution.Add("bisvark", 10)
						fspawndistribution.Add("aardmarck", 5)
						fspawntimedistribution.Add(400, 10)
						fspawntimedistribution.Add(500, 50)
						fspawntimedistribution.Add(1200, 30)
						fspawntimedistribution.Add(2000, 10)
						
						
						factivetimedistribution.Add(1000, 10)
						factivetimedistribution.Add(1500, 15)
						factivetimedistribution.Add(3500, 50)
						factivetimedistribution.Add(4000, 15)
						factivetimedistribution.Add(6000, 10)
					Case 4
						fspawndistribution.Add("aardvark", 55)
						fspawndistribution.Add("bismarck", 25)
						fspawndistribution.Add("bisvark", 10)
						fspawndistribution.Add("aardmarck", 5)
						fspawndistribution.Add("bonus_points", 5)						
						fspawntimedistribution.Add(300, 10)
						fspawntimedistribution.Add(400, 50)
						fspawntimedistribution.Add(800, 30)
						fspawntimedistribution.Add(1800, 10)
						
						
						factivetimedistribution.Add(800, 10)
						factivetimedistribution.Add(1300, 15)
						factivetimedistribution.Add(3000, 50)
						factivetimedistribution.Add(4000, 15)
						factivetimedistribution.Add(6000, 10)					
					Case 5
						fspawndistribution.Add("aardvark", 50)
						fspawndistribution.Add("bismarck", 25)
						fspawndistribution.Add("bisvark", 7)
						fspawndistribution.Add("aardmarck", 8)
						fspawndistribution.Add("bonus_points", 5)
						fspawndistribution.Add("life", 5)					
						fspawntimedistribution.Add(300, 10)
						fspawntimedistribution.Add(400, 50)
						fspawntimedistribution.Add(800, 30)
						fspawntimedistribution.Add(1800, 10)
						
						
						factivetimedistribution.Add(800, 10)
						factivetimedistribution.Add(1300, 15)
						factivetimedistribution.Add(3000, 50)
						factivetimedistribution.Add(4000, 15)
						factivetimedistribution.Add(6000, 10)						
					Default 'anything larger than 5
						fspawndistribution.Add("aardvark", 45)
						fspawndistribution.Add("bismarck", 30)
						fspawndistribution.Add("bisvark", 5)
						fspawndistribution.Add("aardmarck", 10)
						fspawndistribution.Add("bonus_points", 5)
						fspawndistribution.Add("life", 5)					
						fspawntimedistribution.Add(300*1.0/(1.0+(2.0*(Float(pdifficulty)-5.0)/10.0)), 10)
						fspawntimedistribution.Add(400*1.0/(1.0+(2.0*(Float(pdifficulty)-5.0)/10.0)), 50)
						fspawntimedistribution.Add(800*1.0/(1.0+(2.0*(Float(pdifficulty)-5.0)/10.0)), 30)
						fspawntimedistribution.Add(1800*1.0/(1.0+(2.0*(Float(pdifficulty)-5.0)/10.0)), 10)
						
						
						factivetimedistribution.Add(800*1.0/(1.0+(2.0*(Float(pdifficulty)-5.0)/10.0)), 10)
						factivetimedistribution.Add(1300*1.0/(1.0+(2.0*(Float(pdifficulty)-5.0)/10.0)), 15)
						factivetimedistribution.Add(3000*1.0/(1.0+(2.0*(Float(pdifficulty)-5.0)/10.0)), 50)
						factivetimedistribution.Add(4000*1.0/(1.0+(2.0*(Float(pdifficulty)-5.0)/10.0)), 15)
						factivetimedistribution.Add(6000*1.0/(1.0+(2.0*(Float(pdifficulty)-5.0)/10.0)), 10)
				End
						
		End
		setspawnclock()
	End
	
	Method addspace:Void(px#, py#, pID%)
		femptyspaces.AddLast(New GridSpace(px,py, pID))
	End
	
	Method setspawnclock:Void()
		' Generates a random number between 0 and 2*faveragespawntime
'		Local numerator:Float = 0
'		For Local i:Int = 1 To fspawntimecenterweight
'			numerator = numerator + Rnd(faveragespawntime*2)
'		Next
'		fspawnclock.set(numerator/Float(fspawntimecenterweight))
		fspawnclock.set(fspawntimedistribution.Pick())
	End

'	Method setDifficulty:Void(px:Int)
'		difficulty = px	
'	End

	Method circle:List<Triple<String, Int, Float>>(xy:Float[]) 'returns a list of circled targets
		Local result:Triple<String, Int, Float>
		'0: targID, 1:pts, 2: ptsmultiplier
		'Local pts:IntStack = New IntStack
		'Local hitTargs:IntStack = New IntStack
		Local score:Int = 0
		'Local scoremultiplier:Float = 1.0
		'Local out:Int = 0
		Local out:List<Triple<String, Int, Float>> = New List<Triple<String, Int, Float>>
		Local targetscircled:Int = 0
		
		For Local tmpSpace:GridSpace = Eachin fusedspaces
		'	DebugStop()
			If (tmpSpace.circleable())
				If (Collision.PointInPoly(tmpSpace.getx(),tmpSpace.gety(), xy))
					result = tmpSpace.circle()
					'scoremultiplier *= result.f2
					'score += result.f1
					out.AddLast(result)
					'hitTargs.Push(result.f0)
					'targetscircled += 1
					'Print(String(result.f0) + " " + String(result.f1) + " " + String(result.f2))
				End
			End
		End
'		Local hittargsresult:Int[] = pattern(hitTargs)
'		If (result[2]) 'it's a bomb! (metaphorically speaking, probably actually a Bismarck)
'			score = stackSum(negPts) * (multiplier + hittargsresult[0])
'		Else 'not a bomb
'			score = (stackSum(posPts) + stackSum(negPts)) * (multiplier + hittargsresult[0])
'		Endif
		Return out
	End
	
	Method render:Void()
		For Local tmpSpace:GridSpace = Eachin fusedspaces
			tmpSpace.draw()
		End	
	End
End

Class GridSpace
	Field fx#
	Field fy#
	'Field fvisible:Bool
	'Field fcircleable:Bool
	Field fstate:String
	Field ftarget:Target
	Field fid%
	Field ftimer:Timer
'	Field ftarget:Target
	
	Method New (px#, py#, pid:Int)
		fx = px
		fy = py
		fid = pid
		fstate = "inactive"
	End
	
'	Method update:Void()
'		
'	End
	
	Method activate:Void(ptype:String, ptimerstart:Int)
		fstate = "spawn"
		ftarget = New Target(ptype)
		ftimer = New Timer(ptimerstart)
	End

	Method stateexpired:Bool()
		Local out:Bool = False
		Select fstate
			Case "spawn"
				out = ftimer.expired()
			Case "active"
				out = ftimer.expired()
			Case "circled", "timedout"
				out = ftarget.animationfinished(fstate)
		End
		Return out
	End
	
	Method getx:Float()
		Return fx + ftarget.fxtargetoffset
	End
	
	Method gety:Float()
		Return fy + ftarget.fytargetoffset
	End
	
	Method draw:Void()
		ftarget.draw(fx, fy, fstate)
	End
	
'	Method visible:Bool()
'		Local out:Bool = True
'		If (fstate = "burnedout")
'			out = False
'		Endif		
'		Return out	
'	End
	
	Method circleable:Bool()
		Local out:Bool = False
		If (fstate = "active")
			out = True
		Endif		
		Return out
	End
	
	Method statetransition:Void(pnewstate:String, pnewtime%)
		fstate = pnewstate
		ftimer.set(pnewtime)
		ftarget.playsound(fstate)
	End
	
	Method circle:Triple<String, Int, Float>()
		If(fstate = "active")
			statetransition("circled", 10000)
		End		
		Return New Triple<String, Int, Float>(ftarget.ftype,ftarget.fptvalue,ftarget.fptmultiplier)
	End
	
End

Class Font
	'''
	'	class for bitmap fonts. Each font image must be a linear string of the 95 ASCII character images.
	'	Fonts will be monospaced
	'''
	Field fimage:Image
	
	Method New(pimagepath:String)
		fimage = LoadImage(pimagepath, 95)
	End
	
	Method Width:Int()
		Return fimage.Width()
	End

	Method Height:Int()
		Return fimage.Height()
	End

	Method Render:Void(pcode:Int, x:Float, y:Float)
		If pcode < 32 Or pcode > 126
			pcode = 32
		End
		DrawImage(fimage, x, y, pcode-32)
	End
	
End

Function stackSum:Int(stack:IntStack)
	Local sum:Int = 0
	While(stack.Length() > 0)
		sum += stack.Pop()
	Wend
	Return sum
End

Function stackProduct:Int(stack:FloatStack)
	Local product:Float = 0
	While(stack.Length() > 0)
		product *= stack.Pop()
	Wend
	Return product
End

' Count down in milliseconds
Class Timer
	Global timerslist:List<Timer> = New List<Timer>
	Global newlypaused:List<Timer> = New List<Timer>	
	Field ftime:Int
	Field fstart:Int
	Field factive:Bool
	Field fpauseleft:Int
	Global Paused:Bool = False
	
	Method New(milli:Int = 0, active:Bool = True)
		set(milli,active)
		timerslist.AddLast(Self)
	End

	Function Togglepause:Void()
		If (TouchHit(1) Or KeyHit(KEY_P) Or MouseHit(MOUSE_RIGHT))
			If Paused
				Unpause()
			Else
				Pause()
			End
		End
	End
	
	Function Pause:Void()
		PauseMusic()
		For Local tempTimer:Timer = Eachin timerslist
			If tempTimer.factive
				tempTimer.pause()
				newlypaused.AddLast(tempTimer)
			End
		Next
		Paused=True
	End

	Function Unpause:Void()
		ResumeMusic()
		For Local tempTimer:Timer = Eachin newlypaused
			tempTimer.unpause()
		Next
		newlypaused.Clear()
		Paused=False
	End
	
	Method pause:Void()
		If (factive = True)
			fpauseleft = (fstart + ftime) - Millisecs()
			factive = False
		End
	End
	
	Method unpause:Void()
		If (factive = False)
			fstart = Millisecs()
			ftime = fpauseleft
			factive = True
			fpauseleft = 0
		End
	End
	
	Method expired:Bool()
		If (timeleft() > 0)
			Return False
		Else
			Return True
		Endif
	End
	
	Method secsleft:Int()
		Return Int(Ceil(timeleft()/1000))
	End
	
	'in milliseconds
	Method timeleft:Int()
		If (factive)
			Return (fstart+ftime)-Millisecs()
		Else
			Return fpauseleft
		End
	End
	
	Method timepassed:Int()
		If (factive)
			Return Millisecs() - fstart
		Else
			Return ftime - fpauseleft
		End
	End
	
	Method set:Void(milli:Int, active:Bool = True)
		factive = True
		fstart = Millisecs()
		ftime = milli
		fpauseleft = 0
		If (Not(active))
			pause()
		End
	End
	
	Method reset:Void()
		set(ftime)
	End
	
	Method remove:Void()
		timerslist.RemoveFirst(Self) 
	End

End


Class Thread
	Global fdeathtimeout:Int = 2500 'milliseconds to preserve a thread after it has been sunsetted
	
	
	Field points:Stack<Point>
	Field fcolor:Int[3]
	Field fdeathtimer:Timer  'for sunsetting the thread after it is closed


	Method New()
		points = New Stack<Point>
		'fthreads.AddLast(Self)
		fcolor = [255,255,255]
		fdeathtimer = New Timer(fdeathtimeout, False)
	End


	Method pushPoint:Void(pt:Point)
		'printToScreen = String(pt.fx) + " " + String(pt.fy)
		points.Push(New Point(pt.fx, pt.fy))
	End
	
	Method pushPoint:Void(xp:Float,yp:Float)
		points.Push(New Point(xp,yp))
	End
	
	Method clear:Void()
		points.Clear()
	End
	
	Method sunset:Void()
		fdeathtimer.unpause()
		fcolor = [Int(Rnd(256)),Int(Rnd(256)),Int(Rnd(256))]
	End
	

	Method draw:Void()
		SetColor(fcolor[0],fcolor[1],fcolor[2])
		For Local i:Int = 1 To points.Length()-1
			DrawLine(points.Get(i).fx,points.Get(i).fy,points.Get(i-1).fx,points.Get(i-1).fy)
			'printToScreen += String(points.Get(i).fx) + " " + String(points.Get(i).fy)
		Next
		SetColor(255,255,255)
	End
	
	Method tidy:Void(tpt:Point) 'point fq should be the low index of points to move
		'match up last point with earliest point in match segment
		
		'Local tpt:Point = New Point(hits.Get(best).fx,hits.Get(best).fy)
		points.Set(tpt.fq, tpt)
		points.Set(points.Length()-1,tpt)
		
		'remove all points before the hit
		For Local i:Int = 0 To tpt.fq - 1 
			points.Remove(0)
		Next
	End
	
	'return -1 if thread not closed, else the index of the lowest index point in the first intersecting segment, is returned as a point, with fq = the index
	Method closed:Point()
		Local cross:Int[3] 'cross?, x, y
		Local len:Int = points.Length()
'		Local hits:Stack<Int[3]> = New Stack<Int[3]>
		Local hits:Stack<Point> = New Stack<Point>
		Local out:Point = New Point(-1,-1,-1)
		
		If (len > 3)
		
			'find all intersections with the last segment of the thread
			For Local i:Int = 0 To len-4 Step 1
				cross = Point.linesTouch(points.Get(len-1),points.Get(len-2),points.Get(i),points.Get(i+1))			
				If (cross[0] = 1)
					'Print("cross" + " " + String(i))
					hits.Push(New Point(cross[1],cross[2],i))
				Endif
			Next
			
			If (hits.Length() > 0)
				'find hit closest to second to last point in the thread
				Local best:Int = 0
				Local bestDist:Float = 10000000 'impossibly large number
				For Local i:Int = 0 To hits.Length()-1
					Local tpt:Point = New Point(hits.Get(i).fx,hits.Get(i).fy)
					Local dist:Float = Point.dist(tpt,points.Get(len-2))
					If (dist < bestDist)
						best = i
						bestDist = dist
					End
				Next
			out = hits.Get(best)
			End
		End
		
		Return out
	End
	
	Method xy:Float[]()
		Local out:Float[points.Length()*2]
		Local i:Int = 0
		For Local tpt:Point = Eachin points	
			out[i] = tpt.fx
			i += 1
			out[i] = tpt.fy
			i += 1
		Next
		
		Return out
	End

End


Class Threads
	'Const LINE_COLOR:Int = 5
	Const LINE_LEN:Int = 5

	Global fthreads:List<Thread>
	'Global fthread:Thread
	Global circleformed:Bool = False
	Global circlexy:Float[]
	Global flast:Point = New Point(0,0)
	
	
	Function initialize:Void()
		fthreads =  New List<Thread>
		'fthread = New Thread()
		fthreads.AddLast(New Thread())
	End
	
	'Update should be called after pen.update in the main logic loop
	Function update:Void(paused:Bool)
		If (Not paused)
			If pen.down()
				'printToScreen = String(pen.coords.fx) + " " + String(pen.coords.fy) + " " + String(flast.fx) + " " + String(flast.fy)
				If pen.hit()
					flast = New Point(pen.coords.fx, pen.coords.fy)
					fthreads.Last().pushPoint(flast)
				'Else
					'printToScreen = ""
				Elseif(Point.dist(flast,pen.coords) > LINE_LEN)
					fthreads.Last().pushPoint(pen.coords)
					flast = New Point(pen.coords.fx, pen.coords.fy)
				Endif
			Else
				
				fthreads.Last().clear()
			Endif
			
			Local threadclosed:Point = fthreads.Last().closed()
			If (threadclosed.fq >= 0)
				fthreads.Last().tidy(threadclosed)
				
'				Score = Score + Grid.circle()
				circleformed = True
				circlexy = fthreads.Last().xy()
			
				fthreads.Last().sunset()
				fthreads.AddLast(New Thread())
			Else
				circleformed = False
			Endif
			prune()
		End
	'	If (paused) 'Uncomment this if to disable pause shenanigans
	'		fthreads.Last().clear()
	'	End
	End

	'get rid of dead threads that have timed out 'only 1 per loop, but that should do...
	Function prune:Void()
		Local tmpnode:list.Node<Thread> = fthreads.FirstNode()
		If (tmpnode <> Null)
			If (tmpnode.Value().fdeathtimer.expired())
				tmpnode.Remove()
			Endif 
		Endif
	End
	
	Function drawall:Void()
		'printToScreen = ""
		For Local tmpthread:Thread = Eachin fthreads
			'printToScreen += "XXXX "
			tmpthread.draw()
		End
	End


End

Class Render
	'Const DIGIT_WIDTH:Int = 23
	Const MOUSE_GRAPHIC:String = "cursor.png"
	Const FONT_IMAGE:String = "smallerfont_outlined.png"
	Const DIGIT_IMAGE:String = "diginums.png"
	Const PAUSE_IMAGE:String = "Pause.png"
	
	Global fmouseimage:Image
	Global ffontimage:Image
	Global fdigitimage:Image
	Global fpauseimage:Image
	Global fdigitwidth:Int
	Global fdefaultfont:Font
	Global mousexoffset:Float 
	Global mouseyoffset:Float

	
	Function initialize:Void()
		fmouseimage = LoadImage(MOUSE_GRAPHIC)
		mousexoffset = fmouseimage.Width() / 2.0
		mouseyoffset = fmouseimage.Height() / 2.0
		fdigitimage = LoadImage(DIGIT_IMAGE,10)
		fdigitwidth = fdigitimage.Width()
		fdefaultfont = New Font(FONT_IMAGE)
		fpauseimage = LoadImage(PAUSE_IMAGE)
		SetFont(ffontimage)
	End
	
	'
	' draw number using a fixed amount of digits, so magnitude places stay in the same spot
	' x,y is the top left corner of the number
	'
	Function num:Void(num:Int, digits:Int, scale:Float ,x:Float, y:Float)
		Local numstr:String = String(num)
		Local digitspacing:Int = 5
		'DrawText(numstr,100,100)
		For Local i:Int = 1 To digits
			If (numstr.Length() > 0)
				Local digit:Int = Int(numstr[numstr.Length()-1..numstr.Length()])
				'DrawText(numstr[numstr.Length()-1..numstr.Length()],200,200)
				'DrawText(numstr[0..1],200,200)
				'DrawText(String(fdigitimage.Frames()),100,100)
				'DrawText(String(digit),100,100)
				DrawImage(fdigitimage,x+((digits-i)*(fdigitwidth*scale + digitspacing)),y,0,scale,scale,digit)
				numstr = numstr[0..numstr.Length()-1]
			Endif
		Next
	End

	'
	' draw number justified to the right, so magnitude places stay in the same spot
	' x,y is the top right corner of the number
	'
	Function rnum:Void(num:Int, scale:Float ,x:Float, y:Float)
		Local numstr:String = String(num)
		Local digitspacing:Int = 5
		'DrawText(numstr,100,100)
		Local olen:Int = numstr.Length()
		For Local i:Int = 1 To olen
			If (numstr.Length() > 0)
				Local digit:Int = Int(numstr[numstr.Length()-1..numstr.Length()])
				'DrawText(numstr[numstr.Length()-1..numstr.Length()],200,200)
				'DrawText(numstr[0..1],200,200)
				'DrawText(String(fdigitimage.Frames()),100,100)
				'DrawText(String(digit),100,100)
				DrawImage(fdigitimage, x-(i*fdigitwidth*scale + (i-1)*digitspacing),y,0,scale,scale,digit)
				numstr = numstr[0..numstr.Length()-1]
			Endif
		Next
	End
	
	
	Function cursor:Void()
		DrawImage(fmouseimage,TouchX(0)-mousexoffset,TouchY(0)-mouseyoffset)
	End

	Function text:Void(text:String, x:Float, y:Float, scale:Float = 1.0, font:Font = fdefaultfont)
		Local oldcolor:Float[] = GetColor()
		SetColor(255,255,255)
		PushMatrix()
		Scale(scale,scale)
		Local i:Int = 0
		For Local ch:Int = Eachin text.ToChars()
			font.Render(ch,(x/scale)+(i*font.Width()),y/scale)
			i += 1
		Next
		PopMatrix()
		SetColor(oldcolor[0], oldcolor[1], oldcolor[2])
	End

	Function centered_text:Void(text:String, scale:Float = 1.0, font:Font = fdefaultfont)
		PushMatrix()
		'scale = 1.0
		Scale(scale,scale)
		Local y:Float = (Game.HEIGHT/2.0) - (font.Height()/2.0)
		Local x:Float = (Game.WIDTH/2.0) - (Float(text.Length)*font.Width())/2.0
		PopMatrix()
		Render.text(text, x, y, scale, font)
		'Render.text(text, 0,0,scale,font)
	End
	
	Function point_centered_text:Void(text:String, px:Float, py:Float, scale:Float = 1.0, font:Font = fdefaultfont)
		PushMatrix()
		'scale = 1.0
		Scale(scale,scale)
		Local y:Float = py - (font.Height()/2.0)
		Local x:Float = px - (Float(text.Length)*font.Width()*scale)/2.0
		PopMatrix()
		Render.text(text, x, y, scale, font)
		'Render.text(text, 0,0,scale,font)
	End
	
	Function pause:Void()
		DrawImage(fpauseimage, (Game.WIDTH/2)-(fpauseimage.Width()/2), (Game.HEIGHT/2)-(fpauseimage.Height()/2))
	End	
End

Function ResetGame:Void()
	Score = 0
	Lives = STARTINGLIVES
	Level = STARTINGLEVEL
End

Class Game Extends App
	Const UPDATE_RATE:Int = 30

	'Const GAME_MUSIC:String = "mainMusic.ogg"
	Const ROUND_TIME:Int = 40 * 1000
'	Const ROUND_TIME:Int = 10 * 1000
	Const WIDTH:Int = 640
	Const HEIGHT:Int = 480
	
	
	'Field fbackground:Background
	'Field fthread:Thread
	'Field fshowscore:Bool = False
	'Field fgamefont:Image
	
	'Field formed:Bool 'for debug purposes, whether a loop has been formed, if so, stop adding pts
	
	
	'Field guy:Target
	
	Method OnCreate:Int()
		'DiscreteDistribution<Int>.Test()
		Render.initialize()
		pen.initialize()
		'Target.initialize()
		Threads.initialize()
		
		Rooms = New StringMap<Room>
		'create an instance of all rooms here
		Rooms.Set("Load", New LoadRoom)
		Rooms.Set("Title", New TitleRoom)
		Rooms.Set("Lot", New LotRoom)
		Rooms.Set("Gameover", New GameoverRoom)
		Rooms.Set("Highscores", New HighscoresRoom)
		'fthread = New Thread()
		
		SetUpdateRate UPDATE_RATE
		SetMusicVolume( 0.0 ) 'TODO: make this louder
		random.Seed = Millisecs()
		
		ChangeRoom("Title")
		
		global_set_sound_volume(SOUNDEFFECTVOLUME)
		Return(0)
	End
	
	Method OnUpdate:Int() 'Input processing is split among "update" methods (which is ugly and should be changed if there's ever a refactor)
		pen.update(Rooms.Get(ActiveRoom).threadsenabled(), Timer.Paused) 
		
		If (Rooms.Get(ActiveRoom).threadsenabled())
			Threads.update(Timer.Paused)
		Endif

		If (Rooms.Get(ActiveRoom).pauseenabled())
			Timer.Togglepause()
		Endif		
		
		Rooms.Get(ActiveRoom).update()
		Return(0)
	End
	
'	Method processInput:Void()
#rem	
		Select fstate
		Case "title"
			If (pen.down())
				If(mousePt.fx > 12 And mousePt.fx < 102 And mousePt.fy > 426 And mousePt.fy < 466)
					loadRoom("lot")
				Endif
			Endif
		Case "lot"
	
				
		End 
#End	
'	End
	
	
	Method OnRender:Int()
	    'Cls()
		SetAlpha(1) 
	    SetColor(255,255,255)
		
		Rooms.Get(ActiveRoom).render()
		
		'fbackground.draw()
		
		'guy.draw()
'		Grid.draw()			

		If (Rooms.Get(ActiveRoom).threadsenabled())
			Threads.drawall()
		Endif

		If (Rooms.Get(ActiveRoom).mouseenabled())
			Render.cursor()
		End
		'Render.text(String(MouseX()) + " " + String(MouseY()),50,50,3)
		'Render.text("ABCDEFGHIJKLMNOPQRSTUVWXYZ",0,0,1)

		'DrawText(printToScreen,50,50)
		If Timer.Paused
			Render.pause()
		End

		Return(0)
	End
End

Function ChangeRoom:Void(room:String)
	pen.reset()
	Rooms.Get("Load").load(room,"")
	Timer.Unpause()
End

'Class Clock
'	Global milli:Int = 0
'	Function 
'End

Class pen
	Global state:Int = 0 '0 = space toggle off, 1 = space toggle on
	Global fhit:Bool = False
	Global coords:Point
		
	Function initialize:Void()
		coords = New Point(TouchX(0),TouchY(0))
	End
	
	Function update:Void(toggle:Bool, paused:Bool) 'should run every loop
		coords.fx = TouchX(0)
		coords.fy = TouchY(0)
		If (toggle And Not paused)
			fhit = False
			If KeyHit(KEY_SPACE)
				state = (state + 1) Mod 2
				If (state = 1)
					fhit = True
				Endif
			Endif
		End
		
	End
	
	Function down:Bool()
		If (state = 1 Or TouchDown( 0 ))
			Return True
		Else
			Return False
		Endif
	End
	
	Function hit:Bool()
		If (fhit Or (state = 0 And TouchHit( 0 )))
			Return True
		Else
			Return False
		Endif
		
	End
	
	Function reset:Void()
		state = 0
		fhit = False
	End

End

#rem
Class Background
	
	Field fimage:Image
	Field fanimate:Bool
	Field fanimspeed:Int
	
	Method New(image:String, animate:Bool = False, frames:Int = 1, animspeed:Int = 5)
		fimage = LoadImage(image,frames)
		fanimate = animate
		fanimspeed = animspeed
	End
	
	Method draw:Void()
		If (fanimate=False)
			DrawImage(fimage,0.0,0.0)
		Else
			'do crazy stuff
		Endif
	End

End
#end
Function bool_to_string:String(in:Bool)
	Local out:String = "true"
	If Not in
		out = "false"
	Endif
	Return out
End
Function Main:Int()
	'test_threads()
	If html5_target
		Local mp3:Bool = CanPlayMP3()
		Local ogg:Bool = CanPlayOgg()
		If ogg
			'Print("x")
			sound_ext=".ogg"
		Elseif mp3 And Not ogg
			sound_ext=".mp3"
		Endif
	Else
		sound_ext=".ogg"
	Endif
	
	New Game
	Return(0)
End
