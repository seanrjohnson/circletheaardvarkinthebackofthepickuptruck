<?php header("Content-Type: text/plain"); ?>
<?php 
$db = new SQLite3("../scores.sqlite");
$query = <<<EOD
    SELECT name, score FROM scores ORDER BY score DESC LIMIT 10
EOD;

$ret = $db->query($query) or die("Error");
while ($row = $ret->fetchArray(SQLITE3_ASSOC)) {
    echo $row['name'] . "\t" . $row['score'] . "\n";
}

?>